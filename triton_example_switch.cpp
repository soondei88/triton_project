#include "pch.h"
#include <iostream>
using namespace std;

int main() {
	// local variable declaration:
	char grade = 'A';

	switch (grade) {
	case 'A':
		cout << "Excellent!" << endl;
		break;
	case 'B':
		cout << "You got B " << endl;
		break;
	case 'C':
		cout << "Well done" << endl;
		break;
	case 'D':
		cout << "You passed" << endl;
		break;
	case 'F':
		cout << "Better try again" << endl;
		break;
	default:
		cout << "Invalid grade" << endl;
		break;
	}
	cout << "Your grade is " << grade << endl;

	return 0;
}