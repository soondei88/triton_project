import idc
import idaapi
import idautils
import sark

functions = sark.functions()

for func in functions:
    for line in func.lines:
        address = line.ea
        if(sark.is_switch(address)):
            print("found switch : " + str(hex(address)))
