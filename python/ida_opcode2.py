import idc
import ida_bytes
import idaapi
import idautils
import sark
import binascii
import json
import sys
from triton import *


class Triton:

    def __init__(self):
        self.opcodes = None
        self.entry_point = None
        self.function = None
        self.next_address = None
        self.current_function = ""
        self.return_address = {}
        self.count = 0
        self.same_function = []
        self.same_function_count = 0

    def function_change(self, address):
        new_funct = sark.Function(address)
        new_funct_name = new_funct.name
        if self.current_function != new_funct_name:
            self.current_function = new_funct_name
            return True
        else:
            return False

    def get_opcodes(self):
        myCode = Opcode()
        myCode.make_functions()
        myCode.make_opcode_dict()
        myCode.make_next_instruction()
        self.opcodes = myCode
        self.function = myCode.return_opcode_dict()
        self.next_address = myCode.return_next_instruction()

    def input_entry_point(self, entry_point):
        self.entry_point = entry_point

    def input_taget_point(self, target_point):
        self.target_point = target_point

    def bytes_to_int(self, input_bytes):
        result = int(input_bytes.encode('hex'), 16)
        return result

    def taint(self):
        ctx = TritonContext()

        # Set the architecture
        ctx.setArchitecture(ARCH.X86)

        # Symbolic optimization
        ctx.enableMode(MODE.ALIGNED_MEMORY, True)
        ctx.enableSymbolicEngine(True)
        ctx.enableTaintEngine(True)

        # Define the Python syntax
        ctx.setAstRepresentationMode(AST_REPRESENTATION.PYTHON)

        # Define entry point
        pc = self.entry_point

        # Define our input context
        ctx.setConcreteMemoryValue(0x1000, ord('a'))
        ctx.setConcreteMemoryValue(0x1001, ord('b'))
        ctx.setConcreteMemoryValue(0x1002, ord('c'))
        ctx.setConcreteMemoryValue(0x1003, ord('d'))
        ctx.setConcreteMemoryValue(0x1004, ord('e'))

        # Define the serial pointer
        ctx.setConcreteMemoryValue(0x601040, 0x00)
        ctx.setConcreteMemoryValue(0x601041, 0x00)
        ctx.setConcreteMemoryValue(0x601042, 0x90)

        # Define the serial context
        ctx.setConcreteMemoryValue(0x900000, 0x31)
        ctx.setConcreteMemoryValue(0x900001, 0x3e)
        ctx.setConcreteMemoryValue(0x900002, 0x3d)
        ctx.setConcreteMemoryValue(0x900003, 0x26)
        ctx.setConcreteMemoryValue(0x900004, 0x31)

        # Point rdi on our buffer
        ctx.setConcreteRegisterValue(ctx.registers.edi, 0x1000)

        # Setup stack
        ctx.setConcreteRegisterValue(ctx.registers.esp, 0x7fffffff)
        ctx.setConcreteRegisterValue(ctx.registers.ebp, 0x7fffffff)

        # initialize memory
        for line in sark.lines():
            address = line.ea
            ida_memory_value = ida_bytes.get_bytes(address, 1)
            ida_memory_value = self.bytes_to_int(ida_memory_value)
            ctx.setConcreteMemoryValue(address, ida_memory_value)
            if address == 0x412D58:
                print("0x412D58 value : " + str(hex(ctx.getConcreteMemoryValue(address))))
            if address == 0x412D59:
                print("0x412D59 value : " + str(hex(ctx.getConcreteMemoryValue(address))))
            if address == 0x412D5a:
                print("0x412D5a value : " + str(hex(ctx.getConcreteMemoryValue(address))))
            if address == 0x412D5b:
                print("0x412D5b value : " + str(hex(ctx.getConcreteMemoryValue(address))))

        while pc in self.function:
            if pc not in self.same_function:
                self.same_function.append(pc)
            if pc in self.same_function:
                self.same_function_count += 1
            if self.same_function_count == 10000:
                print("10000 times ")
                sys.exit(1)
            # Build an instruction
            inst = Instruction()
            # Setup opcode
            inst.setOpcode(self.function[pc])
            # Setup Address
            inst.setAddress(pc)

            print("=====")
            esbValue = ctx.getConcreteRegisterValue(ctx.registers.esp)
            print("espValue : " + str(hex(esbValue)))
            memoryValue = ctx.getConcreteMemoryValue(esbValue);
            print("memoryValue : " + str(hex(memoryValue)))
            memoryValue = ctx.getConcreteMemoryValue(esbValue+1);
            print("memoryValue : " + str(hex(memoryValue)))
            memoryValue = ctx.getConcreteMemoryValue(esbValue+2);
            print("memoryValue : " + str(hex(memoryValue)))
            memoryValue = ctx.getConcreteMemoryValue(esbValue+3);
            print("memoryValue : " + str(hex(memoryValue)))
            memoryValue = ctx.getConcreteMemoryValue(esbValue+4);
            print("memoryValue : " + str(hex(memoryValue)))

            ori_pc_address = inst.getAddress()
            this_line = sark.Line(ori_pc_address)
            this_line_str = str(this_line)
            this_line_str_split = this_line_str.split()
            if this_line_str_split[1].startswith("call"):
                funct = this_line_str_split[2]
                if not funct.startswith("sub_"):
                    pc = self.next_address[ori_pc_address]
                else:
                    ctx.processing(inst)

                    if inst.getAddress() == self.target_point:
                        ctx.taintRegister(ctx.registers.eax)

                    if inst.isTainted():
                        print '[tainted] %s ' % (str(inst))
                    else:
                        print 'Not tainted %s' % (str(inst))
                    pc = ctx.getConcreteRegisterValue(ctx.registers.eip)
                    try:
                        self.return_address[this_line_str_split[2]] = self.next_address[ori_pc_address]
                    except:
                        self.return_address[this_line_str_split[2]] = 4198516
                        self.count = self.count + 1;
                        if self.count ==2:
                            sys.exit(1)
            else:
                ctx.processing(inst)

                if inst.getAddress() == self.target_point:
                    ctx.taintRegister(ctx.registers.eax)

                if inst.isTainted():
                    print '[tainted] %s ' % (str(inst))
                else:
                    print 'Not tainted %s' % (str(inst))
                pc = ctx.getConcreteRegisterValue(ctx.registers.eip)

                if pc not in self.function:
                    pc = self.return_address[sark.Function(inst.getAddress()).name]

                if pc == inst.getAddress():
                    pc = self.next_address[ori_pc_address]

                if pc == 4202988:
                    pc = 4202990

            # if pc == 2147483643:
            #     pc = 4202265

            print("pc : " + str(pc))
            #SetColor(pc, 1, 0xc02020)
            print("after")
            esbValue = ctx.getConcreteRegisterValue(ctx.registers.esp)
            print("espValue : " + str(hex(esbValue)))
            memoryValue = ctx.getConcreteMemoryValue(esbValue);
            print("memoryValue : " + str(hex(memoryValue)))
            memoryValue = ctx.getConcreteMemoryValue(esbValue+1);
            print("memoryValue : " + str(hex(memoryValue)))
            memoryValue = ctx.getConcreteMemoryValue(esbValue+2);
            print("memoryValue : " + str(hex(memoryValue)))
            memoryValue = ctx.getConcreteMemoryValue(esbValue+3);
            print("memoryValue : " + str(hex(memoryValue)))
            memoryValue = ctx.getConcreteMemoryValue(esbValue+4);
            print("memoryValue : " + str(hex(memoryValue)))
            print("")

        print("program ends")


class Opcode:

    def __init__(self):
        self.functions = None
        self.opcode_dict = {}
        self.next_instruction = {}

    def return_opcode_dict(self):
        return self.opcode_dict

    def return_next_instruction(self):
        return self.next_instruction

    def make_next_instruction(self):
        for func in self.functions():
            lines = func.lines
            address_list = []
            for line in lines:
                address = line.ea
                address_list.append(address)
            address_list.sort()
            address_list_len = len(address_list)
            for addr in address_list:
                place = address_list.index(addr)
                if place != address_list_len-1:
                    next_address = address_list[place + 1]
                    self.next_instruction[addr] = next_address

    def make_functions(self):
        self.functions = sark.functions
        print("finish make functions")

    def make_opcode_dict(self):
        for func in self.functions():
            lines = func.lines
            for line in lines:
                address = line.ea
                address = int(address)
                bytes = line.bytes
                bytes = self.modify_bytes(bytes)
                self.opcode_dict[address] = bytes
        print("finish make opcode dict")

    def modify_bytes(self, byteInput):
        bytes = binascii.hexlify(byteInput)
        bytes = bytes.decode('hex')
        return bytes

    def print_opcode(self):
        for address in self.opcode_dict:
            print(address)
            print(self.opcode_dict[address])
            print("")
        print("print opcode")

    def dump_opcode(self):
        fileName = "opcode.json"
        dump = json.dumps(self.opcode_dict)
        jsonFile = open(fileName, 'w')
        jsonFile.write(dump)
        jsonFile.close()


def main():
    myCode = Triton()
    myCode.input_entry_point(0x406C48)
    myCode.input_taget_point(0x406A29)
    myCode.get_opcodes()
    myCode.taint()


if __name__ == "__main__":
    main()