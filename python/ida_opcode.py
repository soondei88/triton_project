import idc
import ida_bytes
import idaapi
import idautils
import sark
import binascii
import json
import sys
from triton import *


class Triton:

    def __init__(self):
        self.opcodes = None
        self.entry_point = None
        self.target_point = None
        self.target_register = None
        self.function = None
        self.next_address = None

    def get_opcodes(self):
        myCode = Opcode()
        myCode.make_functions()
        myCode.make_opcode_dict()
        myCode.make_next_instruction()
        self.opcodes = myCode
        self.function = myCode.return_opcode_dict()
        self.next_address = myCode.return_next_instruction()

    def input_entry_point(self, entry_point):
        self.entry_point = entry_point

    def input_taget_point(self, target_point):
        self.target_point = target_point

    def bytes_to_int(self, input_bytes):
        result = int(input_bytes.encode('hex'), 16)
        return result

    def taint(self):
        ctx = TritonContext()

        # Set the architecture
        ctx.setArchitecture(ARCH.X86)

        # Symbolic optimization
        ctx.enableMode(MODE.ALIGNED_MEMORY, True)
        ctx.enableSymbolicEngine(True)
        ctx.enableTaintEngine(True)

        # Define the Python syntax
        ctx.setAstRepresentationMode(AST_REPRESENTATION.PYTHON)

        # Define entry point
        pc = self.entry_point

        # Define our input context
        ctx.setConcreteMemoryValue(0x1000, ord('a'))
        ctx.setConcreteMemoryValue(0x1001, ord('b'))
        ctx.setConcreteMemoryValue(0x1002, ord('c'))
        ctx.setConcreteMemoryValue(0x1003, ord('d'))
        ctx.setConcreteMemoryValue(0x1004, ord('e'))

        # Define the serial pointer
        ctx.setConcreteMemoryValue(0x601040, 0x00)
        ctx.setConcreteMemoryValue(0x601041, 0x00)
        ctx.setConcreteMemoryValue(0x601042, 0x90)

        # Define the serial context
        ctx.setConcreteMemoryValue(0x900000, 0x31)
        ctx.setConcreteMemoryValue(0x900001, 0x3e)
        ctx.setConcreteMemoryValue(0x900002, 0x3d)
        ctx.setConcreteMemoryValue(0x900003, 0x26)
        ctx.setConcreteMemoryValue(0x900004, 0x31)

        # Point rdi on our buffer
        ctx.setConcreteRegisterValue(ctx.registers.edi, 0x1000)

        # Setup stack
        ctx.setConcreteRegisterValue(ctx.registers.esp, 0x7fffffff)
        ctx.setConcreteRegisterValue(ctx.registers.ebp, 0x7fffffff)

        # initialize memory
        for line in sark.lines():
            address = line.ea
            ida_memory_value = ida_bytes.get_bytes(address, 1)
            ida_memory_value = self.bytes_to_int(ida_memory_value)
            ctx.setConcreteMemoryValue(address, ida_memory_value)
        
        while pc in self.function:
            # Build an instruction
            inst = Instruction()
            # Setup opcode
            inst.setOpcode(self.function[pc])
            # Setup Address
            inst.setAddress(pc)

            ori_pc_address = inst.getAddress()
            this_line = sark.Line(ori_pc_address)
            this_line_str = str(this_line)
            this_line_str_split = this_line_str.split()
            if this_line_str_split[1].startswith("call"):
                funct = this_line_str_split[2]
                if not funct.startswith("sub_"):
                    pc = self.next_address[ori_pc_address]
                    continue

            if inst.getAddress() == 0x412BF8:
                ctx.setConcreteRegisterValue(ctx.registers.edx, 0x1)
                ctx.concretizeRegister(ctx.registers.edx)
                edxValue = ctx.getConcreteRegisterValue(ctx.registers.edx)
                # print("edxValue : " + str(hex(edxValue)))
                # targetValue = ctx.getConcreteMemoryAreaValue((0x412d54 + edxValue*4), 4)

                # # erase next line : goal
                # #ctx.setConcreteMemoryAreaValue(0x412d54, targetValue)

                # targetValue = self.bytes_to_int(targetValue)
                # print("target value : " + str(hex(targetValue)))

            print("=====")
            esbValue = ctx.getConcreteRegisterValue(ctx.registers.esp)
            print("espValue : " + str(hex(esbValue)))
            memoryValue = ctx.getConcreteMemoryValue(esbValue);
            print("memoryValue : " + str(hex(memoryValue)))
            memoryValue = ctx.getConcreteMemoryValue(esbValue+1);
            print("memoryValue : " + str(hex(memoryValue)))
            memoryValue = ctx.getConcreteMemoryValue(esbValue+2);
            print("memoryValue : " + str(hex(memoryValue)))
            memoryValue = ctx.getConcreteMemoryValue(esbValue+3);
            print("memoryValue : " + str(hex(memoryValue)))
            memoryValue = ctx.getConcreteMemoryValue(esbValue+4);
            print("memoryValue : " + str(hex(memoryValue)))
            ctx.processing(inst)

            print("after")
            esbValue = ctx.getConcreteRegisterValue(ctx.registers.esp)
            print("espValue : " + str(hex(esbValue)))
            memoryValue = ctx.getConcreteMemoryValue(esbValue);
            print("memoryValue : " + str(hex(memoryValue)))
            memoryValue = ctx.getConcreteMemoryValue(esbValue+1);
            print("memoryValue : " + str(hex(memoryValue)))
            memoryValue = ctx.getConcreteMemoryValue(esbValue+2);
            print("memoryValue : " + str(hex(memoryValue)))
            memoryValue = ctx.getConcreteMemoryValue(esbValue+3);
            print("memoryValue : " + str(hex(memoryValue)))
            memoryValue = ctx.getConcreteMemoryValue(esbValue+4);
            print("memoryValue : " + str(hex(memoryValue)))


            if inst.getAddress() == self.target_point:
                ctx.taintRegister(ctx.registers.eax)

            # Print only instructions that are tainted.
            if inst.isTainted():
                print '[tainted] %s' % (str(inst))
            else:
                print 'Not tainted %s' % (str(inst))

            # Next instruction
            pc = ctx.getConcreteRegisterValue(ctx.registers.eip)
            print(hex(inst.getAddress()))
            print(hex(pc))
            # epbValue = ctx.getConcreteRegisterValue(ctx.registers.ebp)
            # print("ebpValue : " + str(hex(epbValue)))
            # memoryValue = ctx.getConcreteMemoryValue(epbValue+4);
            # print("memoryValue : " + str(hex(memoryValue)))

            # if call instruction is not on sub_function... just skip it.
            # ori_pc_address = inst.getAddress()
            # this_line = sark.Line(ori_pc_address)
            # this_line_str = str(this_line)
            # this_line_str_split = this_line_str.split()
            # if this_line_str_split[1].startswith("call"):
            #     funct = this_line_str_split[2]
            #     if not funct.startswith("sub_"):
            #         pc = self.next_address[ori_pc_address]

            if pc == inst.getAddress():
                break

        print("program ends")


class Opcode:

    def __init__(self):
        self.functions = None
        self.opcode_dict = {}
        self.next_instruction = {}

    def return_opcode_dict(self):
        return self.opcode_dict

    def return_next_instruction(self):
        return self.next_instruction

    def make_next_instruction(self):
        for func in self.functions():
            lines = func.lines
            address_list = []
            for line in lines:
                address = line.ea
                address_list.append(address)
            address_list.sort()
            address_list_len = len(address_list)
            for addr in address_list:
                place = address_list.index(addr)
                if place != address_list_len-1:
                    next_address = address_list[place + 1]
                    self.next_instruction[addr] = next_address


    def make_functions(self):
        self.functions = sark.functions
        print("finish make functions")

    def make_opcode_dict(self):
        for func in self.functions():
            lines = func.lines
            for line in lines:
                address = line.ea
                address = int(address)
                bytes = line.bytes
                bytes = self.modify_bytes(bytes)
                self.opcode_dict[address] = bytes
        print("finish make opcode dict")

    def modify_bytes(self, byteInput):
        bytes = binascii.hexlify(byteInput)
        bytes = bytes.decode('hex')
        return bytes

    def print_opcode(self):
        for address in self.opcode_dict:
            print(address)
            print(self.opcode_dict[address])
            print("")
        print("print opcode")

    def dump_opcode(self):
        fileName = "opcode.json"
        dump = json.dumps(self.opcode_dict)
        jsonFile = open(fileName, 'w')
        jsonFile.write(dump)
        jsonFile.close()


def main():
    myCode = Triton()
    myCode.input_entry_point(0x406C48)
    myCode.input_taget_point(0x406C48)
    myCode.get_opcodes()
    myCode.taint()


if __name__ == "__main__":
    main()